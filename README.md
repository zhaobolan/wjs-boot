# 项目介绍
该项目为springboot+mybatis并实现整合多数据源:

* 案例中演示的多数据源是动态加载，适用于多租户中数据库隔离项目
* 项目中还展示了Mybatis如何如何自定义类型处理器
* 项目中还展示了Mybatis中分页插件的使用
* 每次请求时需要在请求头中添加租户编码tenant来实现动态切换数据库
* tenant的值即为租户数据库后缀
* 注意项目测试需要使用POSTMAN，不可直接使用浏览器请求因为需要添加租户编码


# SQL语句准备
### 创建数据库
* wjs_boot、wjs_boot_0000、wjs_boot_yfcs106
### 创建库表

CREATE TABLE `user` (
`id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
`nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
`mobile` varchar(20) DEFAULT NULL COMMENT '手机号',
`password` char(60) DEFAULT NULL COMMENT '密码hash值',
`role` varchar(100) DEFAULT 'user' COMMENT '角色，角色名以逗号分隔',
`create_time` datetime DEFAULT NULL COMMENT '创建时间',
`geom` geometry DEFAULT NULL COMMENT '坐标地址',
`attr` json DEFAULT NULL COMMENT '属性内容',
PRIMARY KEY (`id`),
UNIQUE KEY `mobile_UNIQUE` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='用户表';

INSERT INTO `wjs_boot`.`user` (`id`, `nickname`, `mobile`, `password`, `role`, `create_time`, `geom`, `attr`) VALUES (1, 'wjs_boot', '13512345678', '123', 'user', '2023-04-06 10:04:41', NULL, NULL);
INSERT INTO `wjs_boot`.`user` (`id`, `nickname`, `mobile`, `password`, `role`, `create_time`, `geom`, `attr`) VALUES (2, 'abc2', '13512345677', '123', 'user', '2023-04-06 10:04:42', NULL, NULL);
INSERT INTO `wjs_boot`.`user` (`id`, `nickname`, `mobile`, `password`, `role`, `create_time`, `geom`, `attr`) VALUES (3, 'abc3', '13512345603', '123', 'user', '2023-04-06 10:04:43', NULL, NULL);
INSERT INTO `wjs_boot`.`user` (`id`, `nickname`, `mobile`, `password`, `role`, `create_time`, `geom`, `attr`) VALUES (4, 'abc4', '13512345604', '123', 'user', '2023-04-06 10:04:44', NULL, NULL);
INSERT INTO `wjs_boot`.`user` (`id`, `nickname`, `mobile`, `password`, `role`, `create_time`, `geom`, `attr`) VALUES (5, 'abc5', '13512345605', '123', 'user', '2023-04-06 10:04:45', NULL, NULL);
INSERT INTO `wjs_boot`.`user` (`id`, `nickname`, `mobile`, `password`, `role`, `create_time`, `geom`, `attr`) VALUES (6, 'abc6', '13512345606', '123', 'user', '2023-04-06 10:04:46', NULL, NULL);
INSERT INTO `wjs_boot`.`user` (`id`, `nickname`, `mobile`, `password`, `role`, `create_time`, `geom`, `attr`) VALUES (7, 'abc7', '13512345607', '123', 'user', '2023-04-06 10:04:47', NULL, NULL);
INSERT INTO `wjs_boot`.`user` (`id`, `nickname`, `mobile`, `password`, `role`, `create_time`, `geom`, `attr`) VALUES (8, 'abc8', '13512345608', '123', 'user', '2023-04-06 10:04:48', NULL, NULL);
INSERT INTO `wjs_boot`.`user` (`id`, `nickname`, `mobile`, `password`, `role`, `create_time`, `geom`, `attr`) VALUES (9, 'abc9', '13512345609', '123', 'user', '2023-04-06 10:04:49', NULL, NULL);
INSERT INTO `wjs_boot`.`user` (`id`, `nickname`, `mobile`, `password`, `role`, `create_time`, `geom`, `attr`) VALUES (10, 'abc10', '13512345610', '123', 'user', '2023-04-06 10:04:50', NULL, NULL);

* 提示：所有数据库表都是一样的创建一个后，可以直接复制过去

# 快速开始

### 项目访问地址

* http://127.0.0.1:8090/user/1
  
* http://127.0.0.1:8090/user/listUser


