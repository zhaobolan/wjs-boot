package com.wjs.boot.config.datasource;

import java.util.ArrayList;
import java.util.List;

/**
 * 使用ThreadLocal存储切换数据源后的KEY
 *
 * @author wyj
 * @date 2023/4/7 9:41
 */
public class DataSourceHolder {

    //线程  本地环境
    private static final ThreadLocal<String> dataSources = new InheritableThreadLocal();

    public static List<String> dataSourceIds = new ArrayList<>();

    public final static String dataSource_key  = "wjs_boot";

    //设置数据源
    public static void setDataSource(String datasource) {
        dataSources.set(datasource);
    }

    //获取数据源
    public static String getDataSource() {
        return dataSources.get();
    }

    //清除数据源
    public static void clearDataSource() {
        dataSources.remove();
    }

    public static boolean containsDataSource(String dataSourceId){
        return dataSourceIds.contains(dataSourceId);
    }
}
