package com.wjs.boot.config.datasource;

import com.wjs.boot.util.HeaderUtils;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 动态数据源路由
 *
 * @author wyj
 * @date 2023/4/7 9:43
 */
@Slf4j
public class DynamicDataSource extends AbstractRoutingDataSource {

    /**
     * 返回需要使用的数据源的key，将会按照这个KEY从Map获取对应的数据源（切换）
     */
    @Override
    protected Object determineCurrentLookupKey() {
        // 默认从线程变量取数据库名称
        String dataSourceName = DataSourceHolder.getDataSource();
        if (StringUtils.isEmpty(dataSourceName)) {
            // 继续从请求头获取
            dataSourceName = HeaderUtils.getTenant();
        }
        DataSourceHolder.setDataSource(dataSourceName);
        // 设置多数据源
        if (!DataSourceHolder.containsDataSource(dataSourceName)) {
            Map<Object, Object> targetDataSources = new ConcurrentHashMap<>();
            targetDataSources.put(dataSourceName, createDataSource(dataSourceName));
            super.setTargetDataSources(targetDataSources);
            DataSourceHolder.dataSourceIds.add(dataSourceName);

            //执行afterPropertiesSet方法，完成属性的设置
            super.afterPropertiesSet();
        }

        log.info("当前数据源为：{}", dataSourceName);
        DataSourceHolder.clearDataSource();
        return dataSourceName;
    }


    public HikariDataSource createDataSource(String key) {
        String uriFront = "jdbc:mysql://";
        String host = "localhost:3306";
        String uriEnd = "?serverTimezone=CTT&characterEncoding=utf8&useUnicode=true&useSSL=false" +
                "&autoReconnect=true&zeroDateTimeBehavior=convertToNull&allowMultiQueries=true" +
                "&rewriteBatchedStatements=true&serverTimezone=Asia/Shanghai";

        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setUsername("root");
        hikariDataSource.setPassword("123456");
        hikariDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        hikariDataSource.setJdbcUrl(uriFront + host + "/" + key + uriEnd);
        return hikariDataSource;
    }
}