package com.wjs.boot.util;

import com.wjs.boot.config.datasource.DataSourceHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 从请求头信息中获取数据
 *
 * @author chenjunfan
 */
@Slf4j
public class HeaderUtils {
    public static final String HEADER_TENANT_KEY = "tenant";

    private static HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (Objects.isNull(requestAttributes)) {
            log.info("Request context 不存在");
        }
        return ((ServletRequestAttributes) requestAttributes).getRequest();
    }


    public static String getTenant() {
        return DataSourceHolder.dataSource_key + "_" + getRequest().getHeader(HEADER_TENANT_KEY);
    }

    public static Map<String, String> getHeaders() {
        HttpServletRequest request = getRequest();
        Enumeration<String> headerNames = request.getHeaderNames();
        Map<String, String> headers = new HashMap<>();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headerVal = request.getHeader(headerName);
            headers.put(headerName, headerVal);
        }
        return headers;
    }
}
