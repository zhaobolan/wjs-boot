package com.wjs.boot.entity;

import lombok.Data;

@Data
public class User {
    private Long id;

    private String nickname;

    private String mobile;

    private String password;

    private String role;

    private Long createTime;
}