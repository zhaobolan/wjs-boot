package com.wjs.boot.service;


import com.wjs.boot.entity.User;

import java.util.List;

public interface UserService {
    User getUserById(long userId);

    List<User> listUser(int page, int pageSize);
}
